﻿namespace PartialJson;

/// <summary>
/// Provides
/// </summary>
public interface IJsonPathParser
{
    /// <summary>
    /// Parses passed json path query into json path tokens.
    /// </summary>
    /// <param name="query"> Json path query. </param>
    /// <returns></returns>
    IJsonPathToken[] Parse(string query);
}