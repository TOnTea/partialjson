﻿using System.Text.Json;
using JetBrains.Annotations;
using PartialJson.Internal;

namespace PartialJson;

/// <summary>
/// Deserializer that allows to deserialize only necessary part of JSON.  
/// </summary>
[PublicAPI]
public class PartialJsonDeserializer
{
    /// <summary>
    /// <see cref="PartialJsonDeserializer"/> that uses default JSON path parser.
    /// </summary>
    public static readonly PartialJsonDeserializer Default = new(
        DefaultJsonPathParser.Instance);

    private readonly IJsonPathParser _parser;

    /// <summary>
    /// Initializes new instance of <see cref="PartialJsonDeserializer"/> using specified JSON path parser
    /// </summary>
    /// <param name="parser"> JSON path parser. </param>
    public PartialJsonDeserializer(IJsonPathParser parser)
    {
        _parser = parser;
    }

    /// <summary>
    /// Deserializes UTF-16 JSON sequence into <typeparamref name="T"/> crawling to necessary JSON token using specified JSON path query. 
    /// </summary>
    /// <param name="json"> UTF-16 chars JSON sequence </param>
    /// <param name="query"> JSON path query. </param>
    /// <param name="options"> JSON serializer options. </param>
    /// <typeparam name="T"> Type to deserialize json into. </typeparam>
    /// <returns> Deserialized value. </returns>
    public T? Deserialize<T>(
        ReadOnlySpan<char> json,
        string query,
        JsonSerializerOptions? options = null)
    {
        options ??= JsonSerializerOptions.Default;
        options = new JsonSerializerOptions(options);

        var tokens = _parser.Parse(query);
        var converter = new ByJsonPathJsonConverter<T>(tokens);

        options.Converters.Add(converter);

        _ = JsonSerializer.Deserialize<object>(json, options);

        return converter.Value;
    }

    /// <summary>
    /// Deserializes UTF-8 stream into <typeparamref name="T"/> crawling to necessary JSON token using specified JSON path query. 
    /// </summary>
    /// <param name="utf8Json"> UTF-8 json stream. </param>
    /// <param name="query"> JSON path query. </param>
    /// <param name="options"> JSON serializer options. </param>
    /// <typeparam name="T"> Type to deserialize json into. </typeparam>
    /// <returns> Deserialized value.  </returns>
    public async Task<T?> DeserializeAsync<T>(
        Stream utf8Json,
        string query,
        JsonSerializerOptions? options = null)
    {
        options ??= JsonSerializerOptions.Default;
        options = new JsonSerializerOptions(options);

        var tokens = _parser.Parse(query);
        var converter = new ByJsonPathJsonConverter<T>(tokens);

        options.Converters.Add(converter);

        await JsonSerializer.DeserializeAsync<object>(utf8Json, options);

        return converter.Value;
    }

    /// <summary>
    /// Deserializes UTF-8 stream into <see cref="IAsyncEnumerable{T}"/> crawling to necessary JSON token using specified JSON path query.
    /// </summary>
    /// <param name="utf8Json"> UTF-8 json stream. </param>
    /// <param name="query"> JSON path query. </param>
    /// <param name="options"> JSON serializer options. </param>
    /// <typeparam name="T"> JSON array element type. </typeparam>
    public IAsyncEnumerable<T?> DeserializeAsyncEnumerable<T>(
        Stream utf8Json,
        string query,
        JsonSerializerOptions? options = null)
    {
        options ??= JsonSerializerOptions.Default;
        options = new JsonSerializerOptions(options);

        var tokens = _parser.Parse(query);
        var converter = new ByJsonPathAsyncEnumerableJsonConverter<T>(tokens);

        options.Converters.Add(converter);

#pragma warning disable CA2012
        _ = JsonSerializer.DeserializeAsync<object>(utf8Json, options);
#pragma warning restore CA2012

        return converter.Output.Reader.ReadAllAsync();
    }
}