﻿using System.Text.Json;

namespace PartialJson.Internal;

internal ref struct Utf8JsonCrawler
{
    private readonly IJsonPathToken[] _tokens;
    private int _i;

    public Utf8JsonCrawler(IJsonPathToken[] tokens)
    {
        _tokens = tokens;
        _i = 0;
    }

    public bool Crawl(ref Utf8JsonReader reader)
    {
        var currentType = reader.TokenType;

        while (reader.Read())
        {
            if (reader.TokenType is JsonTokenType.EndObject or JsonTokenType.EndArray)
            {
                return false;
            }

            if (currentType is JsonTokenType.StartObject)
            {
                if (!TryHandleObject(ref reader))
                {
                    return false;
                }

                MoveNext();
            }

            if (currentType is JsonTokenType.StartArray)
            {
                if (!TryHandleArray(ref reader))
                {
                    return false;
                }

                MoveNext();
            }

            if (Matched())
            {
                return true;
            }

            currentType = reader.TokenType;
        }

        return false;
    }

    private bool TryHandleObject(ref Utf8JsonReader reader)
    {
        var currentToken = CurrentToken();

        if (currentToken.IsArrayIndex)
        {
            return false;
        }

        var requiredPropertyName = currentToken.GetPropertyName();

        while (reader.TokenType is not JsonTokenType.EndObject)
        {
            if (reader.ValueTextEquals(requiredPropertyName))
            {
                reader.Read();
                return true;
            }

            reader.Skip(); // Skip to property value
            reader.Skip(); // Skip property value

            if (!reader.Read())
            {
                return false;
            }
        }

        return false;
    }

    private bool TryHandleArray(ref Utf8JsonReader reader)
    {
        var currentToken = CurrentToken();

        if (currentToken.IsObjectProperty)
        {
            return false;
        }

        var requiredIndex = currentToken.GetArrayIndex();
        var currentIndex = 0;

        while (reader.TokenType is not JsonTokenType.EndArray)
        {
            if (currentIndex == requiredIndex)
            {
                return true;
            }

            reader.Skip();
            currentIndex++;

            if (!reader.Read())
            {
                return false;
            }
        }

        return currentIndex == requiredIndex;
    }
    
    private bool Matched()
    {
        return _i == _tokens.Length;
    }

    private void MoveNext()
    {
        _i++;
    }

    private IJsonPathToken CurrentToken()
    {
        return _tokens[_i];
    }
}