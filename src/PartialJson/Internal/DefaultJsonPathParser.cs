﻿namespace PartialJson.Internal;

internal sealed class DefaultJsonPathParser : IJsonPathParser
{
    public static readonly DefaultJsonPathParser Instance = new();
    
    public IJsonPathToken[] Parse(string query)
    {
        var stringTokens = query.Split('.', StringSplitOptions.RemoveEmptyEntries);
        var tokens = new IJsonPathToken[stringTokens.Length];
        for (var i = 0; i < tokens.Length; i++) tokens[i] = new Token(stringTokens[i]);
        return tokens;
    }

    private sealed class Token : IJsonPathToken
    {
        private readonly string _token;

        public Token(string token)
        {
            _token = token;
        }

        public bool IsArrayIndex => _token.StartsWith('[');
        
        public int GetArrayIndex()
        {
            var span = _token.AsSpan();
            span = span[1..];
            span = span[..^1];
            return int.Parse(span);
        }

        public bool IsObjectProperty => !IsArrayIndex;
        
        public string GetPropertyName()
        {
            return _token;
        }
    }
}