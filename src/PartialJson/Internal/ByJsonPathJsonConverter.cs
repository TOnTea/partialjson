﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace PartialJson.Internal;

internal sealed class ByJsonPathJsonConverter<T> : JsonConverter<object>
{
    private readonly IJsonPathToken[] _tokens;

    public ByJsonPathJsonConverter(IJsonPathToken[] tokens)
    {
        _tokens = tokens;
    }

    public T? Value { get; private set; }

    public override object? Read(
        ref Utf8JsonReader reader,
        Type typeToConvert,
        JsonSerializerOptions options)
    {
        var startDepth = reader.CurrentDepth;

        try
        {
            var crawler = new Utf8JsonCrawler(_tokens);

            if (!crawler.Crawl(ref reader))
            {
                return default;
            }

            if (reader.TokenType is JsonTokenType.Null)
            {
                return default;
            }

            Value = JsonSerializer.Deserialize<T>(ref reader, options);
            return default;
        }
        finally
        {
            Compensate.Depth(ref reader, startDepth);
        }
    }

    public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
    {
        throw new NotSupportedException();
    }
}