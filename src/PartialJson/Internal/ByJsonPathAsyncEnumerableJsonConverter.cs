﻿using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Channels;

namespace PartialJson.Internal;

internal sealed class ByJsonPathAsyncEnumerableJsonConverter<T> : JsonConverter<object>
{
    private readonly IJsonPathToken[] _tokens;

    public ByJsonPathAsyncEnumerableJsonConverter(IJsonPathToken[] tokens)
    {
        _tokens = tokens;
    }

    public Channel<T?> Output { get; } = Channel.CreateUnbounded<T?>(new()
    {
        SingleReader = true,
        SingleWriter = true
    });

    public override object? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var startDepth = reader.CurrentDepth;

        try
        {
            var crawler = new Utf8JsonCrawler(_tokens);

            if (!crawler.Crawl(ref reader))
            {
                Output.Writer.Complete();
                return default;
            }

            if (reader.TokenType is JsonTokenType.Null)
            {
                Output.Writer.Complete();
                return default;
            }

            if (reader.TokenType != JsonTokenType.StartArray)
            {
                throw new InvalidOperationException("Specified path does not lead to JSON array");
            }

            reader.Read();
            while (reader.TokenType is not JsonTokenType.EndArray)
            {
                var item = JsonSerializer.Deserialize<T?>(ref reader, options);
                Output.Writer.TryWrite(item);
                reader.Read();
            }

            Output.Writer.Complete();
            return default;
        }
        finally
        {
            Compensate.Depth(ref reader, startDepth);
        }
    }

    public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
    {
        throw new NotSupportedException();
    }
}