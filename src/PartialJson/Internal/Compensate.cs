﻿using System.Text.Json;

namespace PartialJson.Internal;

internal static class Compensate
{
    public static void Depth(
        ref Utf8JsonReader reader,
        int depth)
    {
        switch (reader.TokenType)
        {
            case JsonTokenType.EndObject when reader.CurrentDepth == depth:
            case JsonTokenType.EndArray when reader.CurrentDepth == depth:
                return;
        }

        while (reader.Read())
        {
            switch (reader.TokenType)
            {
                case JsonTokenType.EndObject when reader.CurrentDepth == depth:
                case JsonTokenType.EndArray when reader.CurrentDepth == depth:
                    return;
            }
        }
    }
}