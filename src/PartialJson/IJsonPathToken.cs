﻿namespace PartialJson;

/// <summary>
/// Part of json path query.
/// </summary>
public interface IJsonPathToken
{
    /// <summary>
    /// Is this token an index of array.
    /// </summary>
    bool IsArrayIndex { get; }

    /// <summary>
    /// Gets <see cref="int"/> array index.
    /// </summary>
    /// <returns></returns>
    int GetArrayIndex();

    /// <summary>
    /// Is this token property of object.
    /// </summary>
    bool IsObjectProperty { get; }

    /// <summary>
    /// Gets <see cref="string"/> property name.
    /// </summary>
    /// <returns></returns>
    string GetPropertyName();
}