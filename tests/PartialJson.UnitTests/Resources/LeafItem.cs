﻿using System.Text.Json.Serialization;

namespace PartialJson.UnitTests.Resources;

public sealed class LeafItem
{
    [JsonPropertyName("property_1")] public required string Property1 { get; set; }

    [JsonPropertyName("property_2")] public required string Property2 { get; set; }

    [JsonPropertyName("property_3")] public required string Property3 { get; set; }
}