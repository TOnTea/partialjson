﻿namespace PartialJson.UnitTests.Resources;

public static class EmbeddedResource
{
    public static string Read(string resource)
    {
        using var stream = typeof(EmbeddedResource).Assembly.GetManifestResourceStream(GetFullName(resource));

        using var reader = new StreamReader(stream!);

        return reader.ReadToEnd();
    }

    public static Stream ReadStream(string resource)
    {
        return typeof(EmbeddedResource).Assembly.GetManifestResourceStream(GetFullName(resource))!;
    }

    private static string GetFullName(string resource)
    {
        return $"PartialJson.UnitTests.Resources.{resource}";
    }
}