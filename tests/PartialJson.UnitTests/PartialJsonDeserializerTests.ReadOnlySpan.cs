﻿using PartialJson.UnitTests.Resources;

namespace PartialJson.UnitTests;

public sealed partial class PartialJsonDeserializerTests
{
    [Fact]
    public void Deserializes_Root_Level_Values()
    {
        // Arrange
        var json = EmbeddedResource.Read("LeafJson.json");

        // Act
        var leaf = PartialJsonDeserializer.Default.Deserialize<LeafItem>(json, "leaf")!;

        // Assert
        AssertLeafResult(leaf);
    }

    [Fact]
    public void Deserializes_Nested_Object_Values()
    {
        // Arrange
        var json = EmbeddedResource.Read("LeafJson.json");
        
        // Act
        var leaf = PartialJsonDeserializer.Default.Deserialize<LeafItem>(json, "object.nested")!;
        
        // Assert
        AssertLeafResult(leaf);
    }

    [Fact]
    public void Deserializes_Nested_Array_Values()
    {
        // Arrange
        var json = EmbeddedResource.Read("LeafJson.json");
        
        // Act
        var leaf = PartialJsonDeserializer.Default.Deserialize<LeafItem>(json, "items.[0]")!;
        
        // Assert
        AssertLeafResult(leaf);
    }
    
    [Fact]
    public void Deserializes_Mixed_Nested_Values()
    {
        // Arrange
        var json = EmbeddedResource.Read("LeafJson.json");
        
        // Act
        var leaf = PartialJsonDeserializer.Default.Deserialize<LeafItem>(json, "mixed.[0].object")!;
        
        // Assert
        AssertLeafResult(leaf);
    }

    private static void AssertLeafResult(LeafItem? leaf)
    {
        leaf.Should().NotBeNull();
        leaf?.Property1.Should().Be("item_1");
        leaf?.Property2.Should().Be("item_2");
        leaf?.Property3.Should().Be("item_3");
    }
}