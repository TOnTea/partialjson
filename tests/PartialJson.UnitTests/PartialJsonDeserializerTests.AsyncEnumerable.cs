﻿using PartialJson.UnitTests.Resources;

namespace PartialJson.UnitTests;

public sealed partial class PartialJsonDeserializerTests
{
    [Fact]
    public async Task Deserializes_AsyncEnumerable_Root_Level_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("AsyncEnumerableJson.json");

        // Act
        var items = PartialJsonDeserializer.Default.DeserializeAsyncEnumerable<LeafItem>(stream, "leaf");

        // Assert
        await AssertAsyncEnumerableResultAsync(items);
    }

    [Fact]
    public async Task Deserializes_AsyncEnumerable_Stream_Nested_Object_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("AsyncEnumerableJson.json");
        
        // Act
        var items = PartialJsonDeserializer.Default.DeserializeAsyncEnumerable<LeafItem>(stream, "object.nested");
        
        // Assert
        await AssertAsyncEnumerableResultAsync(items);
    }

    [Fact]
    public async Task Deserializes_AsyncEnumerable_Stream_Nested_Array_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("AsyncEnumerableJson.json");
        
        // Act
        var items = PartialJsonDeserializer.Default.DeserializeAsyncEnumerable<LeafItem>(stream, "items.[0]");
        
        // Assert
        await AssertAsyncEnumerableResultAsync(items);
    }
    
    [Fact]
    public async Task Deserializes_AsyncEnumerable_Stream_Mixed_Nested_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("AsyncEnumerableJson.json");
        
        // Act
        var items = PartialJsonDeserializer.Default.DeserializeAsyncEnumerable<LeafItem>(stream, "mixed.[0].object");
        
        // Assert
        await AssertAsyncEnumerableResultAsync(items);
    }

    private static async Task AssertAsyncEnumerableResultAsync(IAsyncEnumerable<LeafItem?> items)
    {
        var i = 0;

        await foreach (var item in items)
        {
            var localValue = item!;
            
            localValue.Should().NotBeNull();
            localValue.Property1.Should().Be($"{i}_leaf_1");
            localValue.Property2.Should().Be($"{i}_leaf_2");
            localValue.Property3.Should().Be($"{i}_leaf_3");

            i++;
        }
    }
}