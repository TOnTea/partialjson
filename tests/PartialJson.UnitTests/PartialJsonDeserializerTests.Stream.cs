﻿using PartialJson.UnitTests.Resources;

namespace PartialJson.UnitTests;

public sealed partial class PartialJsonDeserializerTests
{
    [Fact]
    public async Task Deserializes_Stream_Root_Level_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("LeafJson.json");

        // Act
        var leaf = (await PartialJsonDeserializer.Default.DeserializeAsync<LeafItem>(stream, "leaf"))!;

        // Assert
        AssertLeafResult(leaf);
    }

    [Fact]
    public async Task Deserializes_Stream_Nested_Object_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("LeafJson.json");
        
        // Act
        var leaf = (await PartialJsonDeserializer.Default.DeserializeAsync<LeafItem>(stream, "object.nested"))!;
        
        // Assert
        AssertLeafResult(leaf);
    }

    [Fact]
    public async Task Deserializes_Stream_Nested_Array_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("LeafJson.json");
        
        // Act
        var leaf = (await PartialJsonDeserializer.Default.DeserializeAsync<LeafItem>(stream, "items.[0]"))!;
        
        // Assert
        AssertLeafResult(leaf);
    }
    
    [Fact]
    public async Task Deserializes_Stream_Mixed_Nested_Values()
    {
        // Arrange
        var stream = EmbeddedResource.ReadStream("LeafJson.json");
        
        // Act
        var leaf = (await PartialJsonDeserializer.Default.DeserializeAsync<LeafItem>(stream, "mixed.[0].object"))!;
        
        // Assert
        AssertLeafResult(leaf);
    }
}